package service.impl;

import annotation.Service;
import service.DemoService;

@Service
public class DemoServiceImpl implements DemoService {

	@Override
	public String test(String name) {
		return "My name is " + name;
	}

}
