package controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import annotation.Autowired;
import annotation.Controller;
import annotation.RequestMapping;
import annotation.RequestParam;
import service.DemoService;

@Controller
@RequestMapping("/demo")
public class DemoController {
	@Autowired
	private DemoService demoService;

	/*
	 * 测试Controller能否正常使用
	 */
	@RequestMapping(value = "/test")
	public void test(@RequestParam(value = "name", required = true) String name, HttpServletRequest request,
			HttpServletResponse response) {
		String result = name;
		out(response, result);
	}

	/*
	 * 测试service能否正常使用
	 */
	@RequestMapping(value = "/testservice")
	public void testservice(@RequestParam(value = "name", required = true) String name, HttpServletRequest request,
			HttpServletResponse response) {
		String result = demoService.test(name);
		out(response, result);
	}

	private void out(HttpServletResponse response, String str) {
		try {
			response.setContentType("application/json;charset=utf-8");
			response.getWriter().print(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
